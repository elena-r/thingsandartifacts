package artifacts;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import org.java_websocket.client.WebSocketClient;
import org.json.JSONArray;
import org.json.JSONObject;

import cartago.*;
import utils.HTTPClient;
import utils.Utils;

/**
 * Artifact that allows to use a HTTP client to initialize Utils.things 
 * with the list of WebThings connected to the Gateway.
 * 
 * @author elena-r
 *
 */
public class WebThingsClient extends Artifact {
	
	void init() {
		System.out.println("initializing WebThingsClient");
	}
	
	/**
	 * Get list of Things hosted on gateway (specified in Utils) 
	 * and save them in Utils.things.
	 */
	@OPERATION
	void getThings() {
		HTTPClient client = new HTTPClient(Utils.gatewayURL + "/things", Utils.token);
		List<JSONObject> thingsList = client.getThings();
		
		if(Utils.things.isEmpty()) {
			thingsList.forEach(thing -> {
				Utils.things.put(thing.getString("title"), thing);
			});
		} else {
			System.out.println("[ERROR] Utils.things is not empty");
		}
		
		System.out.println("Done retrieving Things from gateway");
		
		signal("things_retrieved");
		
	}
	

	

}
