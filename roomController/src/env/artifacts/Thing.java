package artifacts;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import cartago.Artifact;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import utils.HTTPClient;
import utils.ThingWebSocket;
import utils.Utils;

/**
 * Generic Artifact representing a WebThing.
 * It is initialized with a ThingDescription formatted as JSON.
 * 
 * This Artifacts exposes the WebThing's properties,
 * actions and events.
 * 
 * @author elena-r
 *
 */
public class Thing extends Artifact{
	private JSONObject jsonThing;
	private WebSocketClient wsClient;
	private Map<String, JSONObject> actions = new HashMap<>();
	private Map<String, JSONObject> events = new HashMap<>();
	private Map<String, JSONObject> properties = new HashMap<>();
	
	/**
	 * Initialize Thing by retrieving the Thing Description as JSON
	 * from the list of WebThings found at Utils.things.
	 * @param thingTitle The WebThing's title.
	 */
	public void init(String thingTitle) {
		this.jsonThing = Utils.things.get(thingTitle);
		
		// create websocket associated with this Thing
		try {
			wsClient = new ThingWebSocket(new URI(Utils.wsPrefix + jsonThing.getString("href") + "?jwt=" + Utils.token), this);
			try {
				wsClient.connectBlocking();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		setup();
		System.out.println("ThingArtifact \"" + thingTitle + "\" is initialized.");
	}
	
	@OPERATION
	public void setProperty(String propertyName, Object value) {
		JSONObject request = new JSONObject();
		request.put("messageType", "setProperty");
		
		JSONObject propertyData = new JSONObject();
		propertyData.put(propertyName, value);
		request.put("data", propertyData);
		
		this.wsClient.send(request.toString());
	}
	
	@OPERATION
	public void requestAction(String actionName) {
		JSONObject request = new JSONObject();
		request.put("messageType", "requestAction");
		
		JSONObject actionData = new JSONObject();
		actionData.put(actionName, new JSONObject());
		request.put("data", actionData);
		
		this.wsClient.send(request.toString());
	}
	
	@OPERATION
	public void addEventSubscription(String eventName) {
		JSONObject request = new JSONObject();
		request.put("messageType", "addEventSubscription");
		
		JSONObject eventData = new JSONObject();
		eventData.put(eventName, new JSONObject());
		request.put("data", eventData);
		
		this.wsClient.send(request.toString());
	}

	
	public JSONObject getThing() {
		return jsonThing;
	}
	
	
	public void handleProperty(JSONObject data) {		
		if(data.length() == 1) {
			String pName = (String) data.keySet().toArray()[0];
			Object pVal = data.get(pName);
			
			this.beginExternalSession();
			if (getObsProperty(pName).getValue() != pVal) {
				getObsProperty(pName).updateValue(pVal);
			}
			this.endExternalSession(true);
		} else {
			System.out.println("[ERROR] Invalid property data in Thing.handleProperty()");
		}
	}
	
	// can be expanded to manage actionStatus data if needed.
	public void handleAction(JSONObject data) {
		if(data.length() == 1) {
			String aName = (String) data.keySet().toArray()[0];
			JSONObject aVal = data.getJSONObject(aName);
			
			this.beginExternalSession();
			signal(aName);
			this.endExternalSession(true);
		} else {
			System.out.println("[ERROR] Invalid event data in Thing.handleEvent()");
		}
	}
	
	// can be expanded to manage event data if needed.
	public void handleEvent(JSONObject data) {
		if(data.length() == 1) {
			String eName = (String) data.keySet().toArray()[0];
			JSONObject eVal = data.getJSONObject(eName);
			
			this.beginExternalSession();
			signal(eName);
			this.endExternalSession(true);
		} else {
			System.out.println("[ERROR] Invalid event data in Thing.handleEvent()");
		}
		
	}
	
	/*
	 * initialize Map<String, JSONObject> properties, actions, events
	 */
	private void setup() {
		JSONObject p = jsonThing.getJSONObject("properties");
		p.keys().forEachRemaining(key -> {
			this.properties.putIfAbsent(key, p.getJSONObject(key));
		});;
		
		JSONObject a = jsonThing.getJSONObject("actions");
		a.keys().forEachRemaining(key -> {
			this.actions.putIfAbsent(key, a.getJSONObject(key));
		});
		
		JSONObject e = jsonThing.getJSONObject("events");
		e.keys().forEachRemaining(key -> {
			this.events.putIfAbsent(key, e.getJSONObject(key));
		});
		
		HTTPClient pClient = new HTTPClient(Utils.gatewayURL + jsonThing.getString("href") + "/properties", Utils.token);
		JSONObject jsonProperties = pClient.getProperties(); // { pName: pValue, pName:, pValue .... }
		
		//create the ObsProperties
		jsonProperties.keys().forEachRemaining(key -> {
			defineObsProperty(key, jsonProperties.get(key));
		});
	}
	
}