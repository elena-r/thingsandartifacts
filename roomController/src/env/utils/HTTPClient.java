package utils;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * HttpClient to con 
 * 
 * @author elena-r
 *
 */
public class HTTPClient {
	private String url;
	private String token;
	private final HttpClient client;
	
	/**
	 * Create an HTTPClient which will send requests to the specified url.
	 * @param url The url of the webthings gateway.
	 * @param token The authentication token for the gateway.
	 */
	public HTTPClient(String url, String token) {
		this.url = url;
		this.token = token;
		client = HttpClient.newHttpClient();
	}
	
	/**
	 * Get list of JSONObject representing the Web Things residing
	 * on the webthings gateway.
	 * @return The list of Web Things currently connected to the gateway.
	 */
	public List<JSONObject> getThings(){
		List<JSONObject> things = new ArrayList<JSONObject>();
		
		HttpRequest request = HttpRequest.newBuilder()
			      .uri(URI.create(url))
			      .header("Content-Type", "html/text")
			      .header("Accept", "application/json")
			      .header("Authorization", "Bearer " + token) //authentication
			      .build();
		
		client.sendAsync(request, BodyHandlers.ofString())
		      .thenApply(HttpResponse::body)
		      .thenAccept(response -> {
		    	  
		    	  JSONArray jsonArray = new JSONArray(response);
		    	  for(int index : IntStream.range(0, jsonArray.length()).toArray()) {
		    		  things.add(jsonArray.getJSONObject(index));
		    	  }		    	  
		      })
		      .join();
		return things;
	}
	
	/**
	 * Get the properties of the WebThing in JSON format.
	 * @return The properties in JSON format.
	 */
	public JSONObject getProperties() {
		JSONObject jsonProperties = new JSONObject();
		HttpRequest request = HttpRequest.newBuilder()
			      .uri(URI.create(url))
			      .header("Content-Type", "html/text")
			      .header("Accept", "application/json")
			      .header("Authorization", "Bearer " + token) //authentication
			      .build();
		
		client.sendAsync(request, BodyHandlers.ofString())
		      .thenApply(HttpResponse::body)
		      .thenAccept(response -> {
		    	  JSONObject jsonResponse = new JSONObject(response);
		    	  jsonProperties.put("properties", jsonResponse); 	
		      })
		      .join();
		return jsonProperties.getJSONObject("properties");
	}
	
	
	
}