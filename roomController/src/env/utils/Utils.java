package utils;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/**
 * Utility class
 * Modify URLs as needed.
 * @author elena-r
 */
public class Utils {
	public static final String token = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjNlMTRkYWYyLWFlYzgtNGM2Mi04YmUzLWVjYzE4NjA5MDFiMCJ9.eyJjbGllbnRfaWQiOiJsb2NhbC10b2tlbiIsInJvbGUiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZSI6Ii90aGluZ3M6cmVhZHdyaXRlIiwiaWF0IjoxNTk5NzYxNDI5LCJpc3MiOiJodHRwczovL2VsZW5hLXIubW96aWxsYS1pb3Qub3JnIn0.XKBZ9GWHvzfLXLAuxYDsQLQ8cDUl6Xn6Xj2QaLVWJ6KhyDQA7EwL5HB0yGte93n9AvnijwBONHyfqTUahk5JTg";
	public static final String gatewayURL = "https://elena-r.mozilla-iot.org";
	public static final String wsPrefix = "wss://elena-r.mozilla-iot.org";

	/**
	 * Map of (thingTitle, thingJson).
	 */
	public static final Map<String, JSONObject> things = new HashMap<String, JSONObject>();

	public static void printThings() {
		things.forEach((title, thing) -> {
			System.out.println("..::" + title + "::..");
			System.out.println(thing.toString(4));
		});
	}

}