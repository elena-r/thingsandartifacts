package utils;

import java.net.URI;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import artifacts.Thing;

/**
 * This WebSocket allows an Artifact to communicate with
 * the corresponding WebThing connected to the Gateway.
 * 
 * WebSocketClient can be found at
 * https://github.com/TooTallNate/Java-WebSocket
 * 
 * @author elena-r
 *
 */
public class ThingWebSocket extends WebSocketClient{
	private URI serverURI;
	private Thing thing;

	/**
	 * Create Thing's WebSocket
	 * @param serverUri URI of the WebThing (residing in Gateway).
	 * @param thing Reference to the ArtifactThing.
	 */
	public ThingWebSocket(URI serverUri, Thing thing) {
		super(serverUri);
		this.serverURI = serverUri;
		this.thing = thing;
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {		
		System.out.println("[WS] new connection opened to " + serverURI.toString());
	}

	@Override
	public void onMessage(String message) {
		//System.out.println("[WS] received message: " + message);
		
		JSONObject jsonMsg = new JSONObject(message);
		String msgType = jsonMsg.getString("messageType");
		try {
			JSONObject data = jsonMsg.getJSONObject("data");
			if(msgType != null) {
				switch (msgType) {
				case "propertyStatus" : thing.handleProperty(data); break;
				case "actionStatus" : thing.handleAction(data); break;
				case "event" : thing.handleEvent(data); break;
				}
			}
		} catch(JSONException e) {
			//exception thrown for message Types we don't need to consider.
		}

	}
	

	@Override
	public void onClose(int code, String reason, boolean remote) {
		System.out.println("[WS] websocket closed with exit code " + code);
	}

	@Override
	public void onError(Exception ex) {
		System.err.println("[WS_ERROR] an error occurred:" + ex);
		
	}
}