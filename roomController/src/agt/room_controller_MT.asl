!start.

+!start <- getThings. //[WebThingsClient] start httpClient and fetch all things currently on gateway

			
+things_retrieved 
	<- 	.print("Start creating artifacts");
		!create_lamp;
		!create_humiditySensor;
		.print("Done creating artifacts");
		!continue.


+!continue
	<-	addEventSubscription("overheated")[artifact_name("myLamp")];
		.print("Subscribed to \"overheated\" event");
		setProperty("brightness", 30)[artifact_name("myLamp")].


+overheated <- .print("Overheated event!").

+brightness(V) <- .print("Brightness is ", V).

+level(V) : V > 20 <- .print("High humidity Level (", V, ")").
					
+!create_lamp <- makeArtifact("myLamp", "artifacts.Thing", ["My Lamp"], IdLamp);
					focus(IdLamp).
					
+!create_humiditySensor <- makeArtifact("mySensor", "artifacts.Thing", ["My Humidity Sensor"], IdHumiditySensor);
					focus(IdHumiditySensor).
					


{ include("$jacamoJar/templates/common-cartago.asl") }