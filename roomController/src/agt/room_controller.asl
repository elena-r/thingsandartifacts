!start.

+!start <- getThings. //[WebThingsClient] start httpClient and fetch all things currently on gateway

			
+things_retrieved 
	<- 	.print("Start creating artifacts");
		!create_light;
		!create_motionSensor;
		.print("Done creating artifacts").

+motion(true) 
	<- 	.print("MotionSensor detected movement!");
		setProperty("motion", false)[artifact_name("MyMotionSensor")];
		requestAction("toggle")[artifact_name("MyLight")].

+on(V) <- .print("Light value is ", V).

+!create_light
	<-	makeArtifact("MyLight", "artifacts.Thing", ["SimpleLight"], IdLight);
		focus(IdLight).
		
+!create_motionSensor
	<- 	makeArtifact("MyMotionSensor", "artifacts.Thing", ["SimpleMotionSensor"], IdMotionSensor);
		focus(IdMotionSensor).
		


{ include("$jacamoJar/templates/common-cartago.asl") }