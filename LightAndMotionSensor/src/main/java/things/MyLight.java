package things;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mozilla.iot.webthing.Action;
import org.mozilla.iot.webthing.Property;
import org.mozilla.iot.webthing.Thing;
import org.mozilla.iot.webthing.Value;
import org.mozilla.iot.webthing.errors.PropertyError;


public class MyLight extends Thing{
	private static String id = "localhost:8888/things/light";
	private static String title = "SimpleLight";
	private static List<String> type = Arrays.asList("Light"); //mozilla @type
	private static String description = "Simple Light for testing";
	private final Value<Boolean> on;

	public MyLight() {
		super(id, title, new JSONArray(type), description);
		JSONObject onDescription = new JSONObject();
		onDescription.put("@type", "OnOffProperty");
        onDescription.put("title", "On/Off");
        onDescription.put("type", "boolean");
        onDescription.put("description", "Whether the lamp is turned on");
        
        
        this.on = new Value<>(false,
						                // Here, you could send a signal to
						                // the GPIO that switches the lamp
						                // off
						                v -> System.out.printf(
						                        "Light is now %s\n", v));
		
		this.addProperty(new Property(this, "on", on, onDescription));
		
		JSONObject toggleAction = new JSONObject();
		toggleAction.put("@type", "ToggleAction");
		toggleAction.put("title", "ToggleOnOff");
		toggleAction.put("description", "Toggle lamps' OnOff property");
		this.addAvailableAction("toggle", toggleAction, ToggleAction.class);
	}
	
	public static class ToggleAction extends Action{

		public ToggleAction(Thing thing, JSONObject input) {
			super(UUID.randomUUID().toString(), thing, "toggle");
		}
		
		@Override
		public void performAction() {
			Thing thing = this.getThing();
			try {
				thing.setProperty("on", ValueSwitch.getVal());
			} catch (PropertyError e) {
				System.out.println("[PROPERTY ERROR | LIGHT] " + e.getMessage());
			}
		}
	}
	
	public void setLightOff() {
		this.on.set(false);
	}
	
	private static class ValueSwitch {
		private static Boolean val = false;
		
		static Boolean getVal() {
			if(val == true) {
				val = false;
			} else {
				val = true;
			}
			return val;
		}
	}
}