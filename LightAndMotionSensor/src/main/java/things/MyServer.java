package things;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mozilla.iot.webthing.Thing;
import org.mozilla.iot.webthing.WebThingServer;
import org.mozilla.iot.webthing.WebThingServer.ThingHandler.ThingWebSocket;

import fi.iki.elonen.NanoHTTPD.IHTTPSession;

public class MyServer {
	public static boolean isShuttingDown = false;
	
	public static void main(String[] args) {

        List<Thing> things = new ArrayList<>();
        MyLight light = new MyLight();
        MyMotionSensor motionSensor = new MyMotionSensor(Arrays.asList(light));
        
        things.add(light);
        things.add(motionSensor);
        WebThingServer server;
        

        try {
            // If adding more than one thing, use MultipleThings() with a name.
            // In the single thing case, the thing's name will be broadcast.
        	/*
            server = new WebThingServer(new WebThingServer.SingleThing(light),
                                        8888);
                                        */
            
            server = new WebThingServer(new WebThingServer.MultipleThings(things,
                                                                         "LightAndMotionSensor"),
                                       	8888);

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                	MyServer.isShuttingDown = true;
                	motionSensor.setMotionFalse();
                	light.setLightOff();
                    server.stop();
                }
            });

        	motionSensor.setMotionFalse();
        	light.setLightOff();
            server.start(false);
            System.out.println("Server started | hostname: " + server.getHostname() + " | port: " + server.getListeningPort());
    		System.out.println("Write something to simulate movement");
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("[ERROR] Server stopped");
            System.exit(1);
        }
    }
}
