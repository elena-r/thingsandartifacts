package things;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mozilla.iot.webthing.Property;
import org.mozilla.iot.webthing.Thing;
import org.mozilla.iot.webthing.Value;

public class MyMotionSensor extends Thing{
	private static String id = "localhost:8888/things/motionsensor";
	private static String title = "SimpleMotionSensor";
	private static List<String> type = Arrays.asList("MotionSensor"); //mozilla @type
	private static String description = "Simple Motion Sensor for testing";
	
	private final Value<Boolean> motion;
	private List<MyLight> lights;
	
	public MyMotionSensor(List<MyLight> lights) {
		super(id, title, new JSONArray(type), description);
		
		this.lights = lights;
		
		this.setHrefPrefix("/sensor");
		JSONObject motionDescription = new JSONObject();
		motionDescription.put("@type", "MotionProperty");
		motionDescription.put("title", "MotionDetected");
		motionDescription.put("type", "boolean");
		motionDescription.put("description", "Whether the sensor detected movement");
        
        
        this.motion = new Value<>(false,
						                // Here, you could send a signal to
						                // the GPIO that switches the lamp
						                // off
						                v -> System.out.printf(
						                        "Motion is now %s\n", v));
				
		this.addProperty(new Property(this, "motion", motion, motionDescription));
		
		
		new Thread(() -> {
			while(true && !MyServer.isShuttingDown) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String consoleInput;
				try {
					consoleInput = reader.readLine();
					this.motion.set(true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public void setMotionFalse() {
		this.motion.set(false);
	}
	
}