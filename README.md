# TESI #

Argomento: integrazione tra modello ad Agenti (BDI) e Web of Things.
Scadenza upload: 25/09/2020

## Dimostrazione ##
[link drive](https://docs.google.com/document/d/1dNybPrshX9swklBWdrPuxJgPBO9Bzo7QFNcMMIFEMY0/edit?usp=sharing)

## Mozilla Gateway ##
https://iot.mozilla.org/gateway/

### istruzioni Docker ###
    docker pull mozillaiot/gateway
    docker run -d -e TZ=Europe/Rome -v {localPath}/sharedData:/home/node/.mozilla-iot --network="host" --log-opt max-size=1m --log-opt max-file=10 --name webthings-gateway gateway
	docker start webthings-gateway


## Eclipse Projects  ##
\[Jdk14\]

* roomController -> JaCaMo project
* LightAndMotionSensor -> Gradle project
* LampAndHumiditySensor -> Gradle project

### roomController ###
Librerie adoperate:

* [org.json](https://mvnrepository.com/artifact/org.json/json)
* [org.java_websocket](https://github.com/TooTallNate/Java-WebSocket)

I jar sono inclusi nel progetto in /lib/

#### istruzioni ####
nel file /roomController.jcm avviene la creazione di due agenti:

* room_controller_MT : smart room che controlla le WebThings *Lamp* e *HumiditySensor*, esposti dal WebThingServer nel progetto LampAndHumiditySensor
* room_controller : smart room che controlla le WebThings *Light* e *MovementSensor*, esposti dal WebThingServer nel progetto LightAndMotionSensor

Commentare il codice a seconda del server che si ha avviato.

L'URL del gateway mozilla è impostato nella classe /src/env/utils/Utils.java come costante.

### LightAndMotionSensor ###
La mia implementazione di una luce e un sensore di movimento; tramite input da console, si simula la percezione di un movimento.
Il WebServer è hostato su localhost e su porta 8888, ciò si può modificare in MyServer.java

### LampAndHumiditySensor ###
Esempio dal framework Mozilla ([link github](https://github.com/mozilla-iot/webthing-java/blob/master/src/main/java/org/mozilla/iot/webthing/example/MultipleThings.java)) contenente una luce con 2 properties, 1 action, 1 event e un sensore di umidità.
Il WebServer è hostato su localhost e su porta 8888, ciò si può modificare in MultipleThings.java
